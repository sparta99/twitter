<?php

namespace Common\Core;

use Lx\FrontendBundle\Entity\LxRecipeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LxController extends Controller {
    
    /** @var LxWallRepository */
    public function getLxWallRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LxFrontendBundle:LxWall');
        return $return;
    }
}
