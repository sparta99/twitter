<?php

namespace Common\UserBundle\DataFixtures\ORM;

use Common\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;


class UsersFixtures extends AbstractFixture implements ContainerAwareInterface{
    
    private $container;
    
    public function load(ObjectManager $manager){
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        
        $User = new User();
        $password = $encoderFactory->getEncoder($User)->encodePassword('admin', null);
        
        $User->setUsername("admin");
        $User->setPassword($password);
        $User->setEmail("pacyfka99@o2.pl");
        $User->setRoles(array("ROLE_USER"));
        $User->setEnabled(true);
        
        
        $manager->persist($User);
        $manager->persist($User);   
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

}
