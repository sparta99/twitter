<?php

namespace Common\UserBundle\Controller;

//use Symfony\Component\Security\Core\SecurityContextInterface;
//use Symfony\Component\Security\Core\Exception\AuthenticationException;


use Common\UserBundle\Entity\User;
use Common\UserBundle\Exception\UserException;
use Common\UserBundle\Form\Type\LoginType;
use Common\UserBundle\Form\Type\RegisterUserType;
use Common\UserBundle\Form\Type\RememberPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller {

	/**
	 * @Route(
	 *      "/login",
	 *      name = "login"
	 * )
	 * 
	 * @Template()
	 */
	public function loginAction(Request $Request) {
		if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
			return $this->redirectToRoute('wall');
		}
		$authenticationUtils = $this->get('security.authentication_utils');

		// get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();

		// last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();

		$Session = $this->get('session');
		$loginError = "Błędne dane logowania";

		if (isset($error)) {
			$this->get('session')->getFlashBag()->add('danger', $loginError);
		}
		$loginForm = $this->createForm(LoginType::class, array(
			'username' => $Session->get(Security::LAST_USERNAME)
		));

		return array(
			'loginForm' => $loginForm->createView(),
			'error' => $error,
		);
	}

	/**
	 * @Route(
	 *      "/register",
	 *      name = "register"
	 * )
	 * 
	 * @Template()
	 */
	public function registerAction(Request $Request) {
		if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
			return $this->redirectToRoute('wall');
		}
		// Register User Form
		$User = new User();
		$User->setEnabled(true);
//		$error = $authenticationUtils->getLastAuthenticationError();
		$registerUserForm = $this->createForm(RegisterUserType::class, $User);
		if ($Request->isMethod('POST')) {
			$registerUserForm->handleRequest($Request);
			$User->setRoles(array('ROLE_USER'));
			if ($registerUserForm->isValid()) {

				try {
					$userManager = $this->get('user_manager');
					$userManager->registerUser($User);
					$this->get('session')->getFlashBag()->add('success', 'Konto zostało utworzone.');
					return $this->redirect($this->generateUrl('login'));
				} catch (UserException $ex) {
					$this->get('session')->getFlashBag()->add('danger', $ex->getMessage());
				}
			}
		}

		return array(
			'registerUserForm' => $registerUserForm->createView(),
		);
	}


	/**
	 * @Route(
	 *      "/logout",
	 *      name = "logout")
	 */
	public function logoutAction() {
		
	}

}
