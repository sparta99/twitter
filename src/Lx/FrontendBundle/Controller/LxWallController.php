<?php

namespace Lx\FrontendBundle\Controller;

use Common\Core\LxController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LxWallController extends LxController {

	/**
	 * @Route(
	 * "/wall/{page}",
	 * name="wall",
	 * requirements={"page"="\d+"},
	 * defaults={"page"=1}
	 * )
	 * @Template()
	 */
	public function indexAction(Request $request, $page) {

		$form = null;
		if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

			$obj = new \Lx\FrontendBundle\Entity\LxArticle();
			$obj->setUser($this->getUser());
			$form = $this->createForm(\Lx\FrontendBundle\Form\LxArticleType::class, $obj);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				try {
					$em = $this->getDoctrine()->getManager();
					$em->getConnection()->beginTransaction();
					$wall = new \Lx\FrontendBundle\Entity\LxWall;
					$wall->setArticle($obj);
					$em->persist($wall);
					$em->persist($obj);
					$em->flush();
					$em->getConnection()->commit();
					$this->get('session')->getFlashBag()->add('success', 'Wiadomość została wysłana');
					return $this->redirectToRoute('wall');
				} catch (Exception $e) {
					$em->getConnection()->rollBack();
					throw $this->createNotFoundException('Wiadomość nie została wysłana');
				}
			}
			$form = $form->createView();
		}

		$wallArticles = $this->getLxWallRepository()->getWallArticles();
	
		$limit = $request->query->getInt('limit', $this->container->getParameter('pagination_limit'));
		$paginator = $this->get('knp_paginator');
		$wallArticles = $paginator->paginate($wallArticles, $page, $limit);

		return $this->render('LxFrontendBundle:Wall:index.html.twig', [
					'wallArticles' => $wallArticles,
					'form' => $form,
		]);
	}

}
