<?php

namespace Lx\FrontendBundle\Form;

use Lx\FrontendBundle\Entity\LxArticle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class LxArticleType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$disabled = false;
		$save = 'Zapisz';
		if ($options['form_type'] == 'delete')
			$disabled = true;

		$builder->add('body', TextareaType::class, array(
			'label' => "Treść",
			'required' => false,
			'disabled' => $disabled,
		));
	
		$builder->add('save', SubmitType::class, array('label' => $save));

	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxArticle::class,
			'form_type' => null,
		));
	}

	public function getName() {
		return 'articleForm';
	}

}
