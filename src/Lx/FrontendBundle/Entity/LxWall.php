<?php

namespace Lx\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lx_wall")
 * @ORM\Entity(repositoryClass="Lx\FrontendBundle\Repository\LxWallRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 */
class LxWall {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Lx\FrontendBundle\Entity\LxArticle
	 *
	 * @ORM\ManyToOne(targetEntity="Lx\FrontendBundle\Entity\LxArticle")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="article", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $article;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=true)
	 */
	private $createdAt;


	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function preSave() {
		if (!isset($this->createdAt)) {
			$this->setCreatedAt(new \DateTime());
		}
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param \Lx\FrontendBundle\Entity\LxArticle $article
     *
     * @return LxWall
     */
    public function setArticle(\Lx\FrontendBundle\Entity\LxArticle $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Lx\FrontendBundle\Entity\LxArticle
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LxWall
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
