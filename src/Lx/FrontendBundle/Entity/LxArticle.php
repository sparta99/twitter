<?php

namespace Lx\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lx_article")
 * @ORM\Entity(repositoryClass="Lx\FrontendBundle\Repository\LxArticleRepository")
 * @ORM\HasLifecycleCallbacks
 */
class LxArticle {

	const status_pending = "pending";
	const status_published = "published";
	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Common\UserBundle\Entity\User
	 *
	 * @ORM\ManyToOne(targetEntity="Common\UserBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $user;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="body", type="text", nullable=false)
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $body;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="status", type="string", length=127, nullable=false)
	 */
	private $status = self::status_published;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=true)
	 */
	private $createdAt;


	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function preSave() {
		if (!isset($this->createdAt)) {
			$this->setCreatedAt(new \DateTime());
		}
	}
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return LxArticle
     */
    public function setBody($body)
    {
        $this->body = htmlspecialchars($body);

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return LxArticle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LxArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Common\UserBundle\Entity\User $user
     *
     * @return LxArticle
     */
    public function setUser(\Common\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Common\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
