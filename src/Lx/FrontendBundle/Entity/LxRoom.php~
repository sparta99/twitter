<?php

namespace Lexus\RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lx_room")
 * @ORM\Entity(repositoryClass="Lexus\RecipeBundle\Repository\LxRoomRepository")
 * 
 */
class LxRoom {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="Lexus\RecipeBundle\Entity\LxBed", mappedBy="room", cascade={"persist"})
	 */
	protected $beds;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return LxRoom
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->beds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bed
     *
     * @param \Lexus\RecipeBundle\Entity\LxBed $bed
     *
     * @return LxRoom
     */
    public function addBed(\Lexus\RecipeBundle\Entity\LxBed $bed)
    {
        $this->beds[] = $bed;

        return $this;
    }

    /**
     * Remove bed
     *
     * @param \Lexus\RecipeBundle\Entity\LxBed $bed
     */
    public function removeBed(\Lexus\RecipeBundle\Entity\LxBed $bed)
    {
        $this->beds->removeElement($bed);
    }

    /**
     * Get beds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeds()
    {
        return $this->beds;
    }
}
